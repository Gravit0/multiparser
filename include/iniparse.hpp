#include <iostream>
#include <fstream>
#include <string>
#pragma once
namespace iniparse
{
std::string get(std::fstream* file,std::string key,std::string space);
void set(std::string filename, std::string key, std::string space, std::string value);
}

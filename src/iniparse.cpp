#include "iniparse.hpp"
#include "main.hpp"
#include <string>

using namespace std;
namespace iniparse
{
std::string get(fstream* file,string key,string space)
{
    bool isCheckSpace=true;
    if(space.empty()) isCheckSpace=false;
    std::string str;
    if(isCheckSpace)
    while((*file))
    {
        //file >> str;
        getline(*file,str,'\n');
        if(str.substr(0,space.size())==space) break;
    }
    while((*file))
    {
        std::string str2;
        getline(*file,str2,'\n');
        auto i = str2.find('=');
        std::string first = str2.substr(0,i);
        if(DeleteChars(first,' ')==key)
        {
            std::string secound = str2.substr(i+1,str2.size());
            return secound;
            break;
        }
    }
    return "";
}
void set(std::string filename,string key,string space,string value)
{
    std::fstream fileR;
    std::fstream* file = &fileR;
    fileR.open(filename, std::ios_base::in);
    if(!fileR)
    {
        std::cerr << "File not open\n";
        return;
    }
    bool isCheckSpace=true;
    if(space.empty()) isCheckSpace=false;
    std::string fullfile;
    getline(*file,fullfile,'\0');
    int i=0,filesize=fullfile.size();
    if(isCheckSpace)
    {

        while(i<filesize)
        {
            if(fullfile.substr(i,space.size())==space) break;
            int nextindex = fullfile.find('\n',i);
            if(nextindex>0)
            {
                i=nextindex;
            }
        }
    }
    while(i<filesize)
    {
        int nextindex = fullfile.find('\n',i+1);
        int it = fullfile.find('=',i);
        //std::cout << i << " " << it << " " << nextindex << " " << filesize << std::endl;
        if(it>nextindex)
        {
            i=nextindex;
            continue;
        }
        if(it>0)
        {
            std::string first = fullfile.substr(i+1,it - i - 1);
            //std::cout << first << std::endl;
            if(first==key || first+" "==key)
            {
                //std::cout << "OK";
                //std::cout << i << " " << it << " " << nextindex << " " << filesize << std::endl;
                //std::string secound = str2.substr(it+1,fullfile.size());
                fullfile.replace(it+1,nextindex,value);
                //std::cout << fullfile;
                file->close();
                std::fstream file2;
                file2.open(filename, std::ios_base::out);
                if(!file2)
                {
                    std::cerr << "File not open\n";
                    return;
                }
                file2.write(fullfile.data(),fullfile.size());
                break;
            }
        }
        if(nextindex>0)
        {
            i=nextindex;
        }
        else break;
    }
}
}

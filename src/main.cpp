#include <iostream>
#include "iniparse.hpp"
#pragma once
std::string DeleteChars(const std::string str,char ch)
{
    std::string tmp = str;
    size_t size = tmp.size();
    for(unsigned int i=tmp.find(ch);i>0 && i < size;i=tmp.find(ch,i+1))
        {
            tmp.replace(i,1,"");
            i++;
        }
    return tmp;
}
int main(int argc, char** argv)
{
    
    if(argc>1)
    {
        if(std::string(argv[1])=="ini")
        {
            if(argc==5 || argc==6) { if(std::string(argv[2])=="get")
            {
               // std::cout << argc;
                std::fstream file;
                file.open(argv[3], std::ios_base::in);
                if(!file)
                {
                    std::cerr << "File not open\n";
                    return 1;
                }
                if(argc==6)
                    std::cout<< iniparse::get(&file,argv[4],"["+std::string(argv[5])+"]");
                else
                    std::cout << iniparse::get(&file,argv[4],"");
                file.close();
            }}
            else if(argc==6 || argc==7) { if(std::string(argv[2])=="set")
            {
               // std::cout << argc;
                if(argc==6)
                    iniparse::set(argv[3],argv[4],"["+std::string(argv[5])+"]",argv[7]);
                else
                    iniparse::set(argv[3],argv[4],"",argv[6]);
            }}
        }
    }
}
